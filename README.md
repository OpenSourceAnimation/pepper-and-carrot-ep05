# Source files of **Pepper & Carrot Motion Comic - Episode 5**.

IMPORTANT: This repository uses Git LFS (https://git-lfs.github.com/). Please make sure to install it before you clone.

To build sources of this animation you will need following software installed:

- Blender 2.79b (https://download.blender.org/release/Blender2.79/)
- Blender 2.83.18 - for editing video sequence files only - project-en.blend / project-ru.blend (https://download.blender.org/release/Blender2.83/)
- CoaTools plugin for Blender. Please make sure to use version 1.0.4 - https://github.com/ndee85/coa_tools/releases/tag/v1.0.4
- RenderChan (https://morevnaproject.org/renderchan/)

NOTE: All those applications are free software, so you can download and use them for free.

Rendering files:

```
renderchan ~/peppercarrot-ep05/project-en.blend
```

When rendering is completed you will find resulting file in "render" subtdirectory - `~/peppercarrot-ep05/render/project-en.blend.avi`.

NOTE: It is assumed you unpacked project sources into “~/peppercarrot-ep05/” directory.

For more details about working with sources of this animation see detailed instructions for other episode - https://morevnaproject.org/2017/08/12/work-sources-pepper-carrot-episode-6/

If you plan to contribute into this repository, please consider to read important instructions here - https://gitlab.com/OpenSourceAnimation/pepper-and-carrot-ep05/-/blob/master/CONTRIBUTING.md
