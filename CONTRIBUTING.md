Contributions to this repository are accepted via GitLab's PRs - https://gitlab.com/OpenSourceAnimation/pepper-and-carrot-ep05/

If you wish your name to appear in the credits, please make sure to edit "CREDITS.txt" file as part of your PR (see "Community contributions" section at the end).

If you will not edit "CREDITS.txt" file as part of your PR, then we will assume that you providing your contribution under CC-0 license and your name will not be attributed in final credits.
